def min_et_max(tab):
    assert len(tab)!=0, 'Tableau vide'
    i_min=tab[0]
    i_max=tab[0]
    for i in tab:
        if i<i_min:
            i_min=i
        if i>i_max:
            i_max=i
    return {'min':i_min,'max':i_max}

assert min_et_max([0, 1, 4, 2, -2, 9, 3, 1, 7, 1]) == {'min': -2, 'max': 9} 
assert min_et_max([0, 1, 2, 3]) == {'min': 0, 'max': 3} 
 
assert min_et_max([3]) == {'min': 3, 'max': 3} 
assert min_et_max([1, 3, 2, 1, 3]) == { 'min': 1, 'max': 3} 
assert min_et_max([-1, -1, -1, -1, -1]) == {'min': -1, 'max': -1}








class Carte:
    def __init__(self, c, v):
        """ Initialise les attributs couleur (entre 1 et 4), et valeur (entre 1 et 13). """
        self.couleur = c
        self.valeur = v

    def get_valeur(self):
        """ Renvoie la valeur de la carte : As, 2, ..., 10, Valet, Dame, Roi """
        valeurs = ['As','2', '3', '4', '5', '6', '7', '8', '9', '10', 'Valet', 'Dame', 'Roi']
        return valeurs[self.valeur - 1]

    def get_couleur(self):
        """ Renvoie la couleur de la carte (parmis pique, coeur, carreau, trèfle). """
        couleurs = ['pique', 'coeur', 'carreau', 'trèfle']
        return couleurs[self.couleur - 1]

class Paquet_de_cartes:
    def __init__(self):
        """ Initialise l'attribut contenu avec une liste des 52 objets Carte possibles
            rangés par valeurs croissantes en commençant par pique, puis coeur,
            carreau et tréfle. """
        self.contenu=[Carte(c,v) for c in range(1,5) for v in range(1,14)]

    def get_carte(self, pos):
        """ Renvoie la carte qui se trouve à la position pos (entier compris entre 0 et 51). """
        assert pos<=51, "paramètre pos impossible"
        return self.contenu[pos]
    
jeu = Paquet_de_cartes() 
 

        
        
        
        
        
        
        
        
        