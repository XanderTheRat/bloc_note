################ Exercice 5 ##################

##### PARTIE A #######

# QUESTION 1
# a.

def fonction1(tab, i):
    nb_elt=len(tab)
    cpt=0
    for j in range(i+1, nb_elt) :
        if tab[j]<tab[i] :
            cpt+=1
    return cpt

# les 3 appels de la question a. (avec des print)
print(fonction1([1, 5, 3, 7], 0)) #0
print(fonction1([1, 5, 3, 7], 1)) #1
print(fonction1([1, 5, 2, 6, 4], 1)) #2

# b.   Le nombre d'elt dans le tab qui est inf à tab[i], soit le nombre d'elt qui est inf àl'elt d'indice i

# QUESTION 2
def nombre_inversions(tab):
    nbr_inversion = 0
    for i in range (len(tab)) :
        nbr_inversion = nbr_inversion + fonction1(tab, i)
    return nbr_inversion

assert nombre_inversions([1, 5, 7]) == 0
assert nombre_inversions([1, 6, 2, 7, 3]) == 3
assert nombre_inversions([7, 6, 5, 3]) == 6

# QUESTION 3
#   quadratique

##### PARTIE B #######

# QUESTION 1
#   tri fusion

# QUESTION 2

# la fonction tri est donnée (saurez-vous reconnaître ce tri ? Et donner sa complexité ?) Fusion, nlog2n
def tri(tab):
    if tab == []:
        return []
    return inserer_elt_dans_liste_triee ( tab [0], tri( tab [1:]) )

# accompagnée d'une fonction auxiliaire, bien sûr
def inserer_elt_dans_liste_triee (elt_a_inserer, liste ):
    if liste == []:
        return [elt_a_inserer]
    if elt_a_inserer <= liste[0]:
        return [elt_a_inserer]+liste
    return [liste [0]]+ inserer_elt_dans_liste_triee (elt_a_inserer, liste [1:])

def moitie_gauche(tab):
    if len(tab)%2==1:
        x=len(tab)//2
        return tab[0:x+1]
    if len(tab)%2==0 :
        x=len(tab)//2
        return tab[0:x]

assert moitie_gauche([]) == []
assert moitie_gauche([4, 8, 3]) == [4, 8]
assert moitie_gauche ([4, 8, 3, 7]) == [4, 8]

def moitie_droite(tab):
    if len(tab)%2==1:
        x=len(tab)//2
        return tab[x+1:]
    if len(tab)%2==0 :
        x=len(tab)//2
        return tab[x:]

assert moitie_droite([]) == []
assert moitie_droite([4, 8, 3]) == [3]
assert moitie_droite ([4, 8, 3, 7]) == [3,7]


# QUESTION 3

# la fonction nb_inv_tab(tab1, tab2) est donnée :
def nb_inv_tab(tab1, tab2) :
    """total_inv = 0
    for elt1 in tab1:
        for elt2 in tab2:
            if elt1> elt2:
                total_inv +=1
    return total_inv"""
    cpt = 0
    cpt_int = 0
    i = 0
    j = 0
    while i < len(tab1) and j < len(tab2):
        if tab1[i] > tab2[j]:
            cpt_int += 1
            j = j + 1
        else:
            i = i + 1
            cpt = cpt + cpt_int
    while i < len(tab1):
       i = i + 1
       cpt = cpt + cpt_int
    return cpt


assert nb_inv_tab([3, 7, 9], [2, 10]) == 3
assert nb_inv_tab([7, 9, 13], [7, 10, 14]) == 3


def nb_inversions_rec(tab):
    if len(tab)<2:
        return 0
    tg=moitie_gauche(tab)
    td=moitie_droite(tab)
    return nb_inversions_rec(tg)+nb_inversions_rec(td)+nb_inv_tab(tri(tg),tri(td))
        

assert nb_inversions_rec([1, 5, 7]) == 0
assert nb_inversions_rec([1, 6, 2, 7, 3]) == 3
assert nb_inversions_rec([7, 6, 5, 3]) == 6



