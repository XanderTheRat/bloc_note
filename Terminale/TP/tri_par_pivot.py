def partionner(t):
    ind,n = 0,len(t)
    fin,i = n-1,1
    while not i > fin:
        if t[i]<t[ind]:
            t[i],t[ind]=t[ind],t[i]
            ind=i
            i+=1
        else:
            t[i],t[fin]=t[fin],t[i]
            fin-=1
    return ind

def tri_pivot(liste):
    if len(liste)<2:
        return liste
    i=partionner(liste)
    return tri_pivot(liste[:i]) + [liste[i]] +  tri_pivot(liste[i+1:])






assert tri_pivot ([5,3,2,6,4,1,3,7]) == [1, 2, 3, 3, 4, 5, 6, 7], 'erreur test 1'
assert tri_pivot ([5,3,2,6,4,1,3]) == [1, 2, 3, 3, 4, 5, 6], 'erreur test 2'
assert tri_pivot ([]) == [], 'erreur test 3'
assert tri_pivot ([5]) == [5], 'erreur test 4'
assert tri_pivot ([2,1]) == [1, 2], 'erreur test 1'