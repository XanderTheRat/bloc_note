# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 09:34:28 2024

@author: sdelmart
"""
from tkinter import *
import pygame
from time import *
import tkinter.simpledialog as simpledialog # --- #D003 -- Regle les problemes au cas ou les simpledialog ne marchent pas

# Variables globales
numero_niveau = 0
fini = False
deplacements = 0  # Scotty
Dep_glob = 0 # Martin - Annonce et sauvegarde le nombre global de déplacement sur tout les niveaux. Réinitialiser le nombre de dep ou rémarrer le niveau ne le touchera pas
# ------------ # Martin - Variables calculant le temps mit pour finir un niveau - Nécessaire afin de calculer le score final
temps_niv_1=0
temps_niv_2=0
temps_niv_3=0 
# Ces trois variable vont calculer le temps local que prend le joueur à faire un niveau.
temps_tot = 0
# -------------
points = 0
# --- #D003 -- Listes des musiques disponibles, le joueur viendra choisir laquelle il veut parmit les musiques proposées
musique={
    'Sokoban':'musiques/background_music.mp3', 
    }
musique_active=musique['Sokoban'] # --- #D003 -- Musique actuellement active ( en placeholder le temps de )

#Initialisation de pygame mixer Scotty
pygame.mixer.init()


#Charger la musique de fond Scotty
pygame.mixer.music.load(f"{musique_active}")  #Scotty
pygame.mixer.music.play(-1)  #-1 fait jouer la musique à l'infini #Scotty


# Variable pour suivre l'état actuel du son (Scotty)
son_active = True


# Fonction pour couper ou rétablir le son (Scotty)
def toggle_mute():
    global son_active
    if son_active:
        pygame.mixer.music.set_volume(0.0)  # Met le volume à 0 pour couper le son
    else:
        pygame.mixer.music.set_volume(1.0)  # Rétablit le volume à 1
    son_active = not son_active
    




# Fonction pour arrêter la musique (Scotty)
def arreter_musique():
    pygame.mixer.music.stop()



# Création de la fenêtre principale
Mafenetre = Tk()
Mafenetre.title('Sokoban Scott, Mathieu, Simon, Martin')


# Création du plateau
plateau = []
for i in range(12):
    plateau.append([])
    for j in range(16):
        plateau[i].append([])
        for k in range(4):
            plateau[i][j].append(0)

#Choix de base des touches
key_bindings = {
    'Up': 'Up',
    'Down': 'Down',
    'Left': 'Left',
    'Right': 'Right',
    'Restart': 'r',
    'Quit': 'q',
}
           
            
          
            
          
def set_key_bindings():
    global key_bindings
    key_bindings['Up'] = simpledialog.askstring("Key Binding", "Enter key for 'Up':", initialvalue=key_bindings['Up'])
    key_bindings['Down'] = simpledialog.askstring("Key Binding", "Enter key for 'Down':", initialvalue=key_bindings['Down'])
    key_bindings['Left'] = simpledialog.askstring("Key Binding", "Enter key for 'Left':", initialvalue=key_bindings['Left'])
    key_bindings['Right'] = simpledialog.askstring("Key Binding", "Enter key for 'Right':", initialvalue=key_bindings['Right'])
    key_bindings['Restart'] = simpledialog.askstring("Key Binding", "Enter key for 'Restart':", initialvalue=key_bindings['Restart'])
    key_bindings['Quit'] = simpledialog.askstring("Key Binding", "Enter key for 'Quit':", initialvalue=key_bindings['Quit'])
 
 
# Niveau 1
# Murs
def genre_niveau_1():
    global temps_niv_1
    for i in range(12):
        plateau[i][0][0] = 1
        plateau[i][1][0] = 1
        plateau[i][14][0] = 1
        plateau[i][15][0] = 1

    for j in range(16):
        plateau[0][j][0] = 1
        plateau[1][j][0] = 1
        plateau[10][j][0] = 1
        plateau[11][j][0] = 1
        # Joueur
    plateau[3][3][1] = 1
    # Caisses
    plateau[5][4][2] = 1
    plateau[5][6][2] = 1
    # Interrupteurs
    plateau[3][8][3] = 1
    plateau[5][5][3] = 1
    temps_niv_1 = time_ns()
genre_niveau_1()




# Fonctions, appelées au bon moment, pour les niveaux suivants
def genere_niveau_2():
    global temps_niv_2
    # On efface le plateau
    for i in range(2, 10):
        for j in range(2, 14):
            plateau[i][j][0] = 0
            plateau[i][j][1] = 0
            plateau[i][j][2] = 0
            plateau[i][j][3] = 0
    # On crée un nouveau plateau
    # Murs
    plateau[8][8][0] = 1
    plateau[7][3][0] = 1
    # Joueur
    plateau[9][9][1] = 1
    # Caisses
    plateau[5][4][2] = 1
    plateau[5][6][2] = 1
    # Interrupteurs
    plateau[3][8][3] = 1
    plateau[5][5][3] = 1
    temps_niv_2 = time_ns()

def genere_niveau_3():
    for i in range(2, 10):
        for j in range(2, 14):
            plateau[i][j][0] = 0
            plateau[i][j][1] = 0
            plateau[i][j][2] = 0
            plateau[i][j][3] = 0
    temps_niv_3 = time_ns

# Fonction testant si un niveau est fini
def test_victoire():
    for i in range(12):
        for j in range(16):
            if plateau[i][j][3] == 1 and plateau[i][j][2] == 0:
                return False
    return True


# Fonction pour réinitialiser le compteur de déplacements (Scotty)
def reset_counter():
    global deplacements  # Rendre les déplacements égaux dans tout le programme
    deplacements = 0
    update_deplacements_label()
    affiche_deplacements_canevas()


# Fonction pour mettre à jour l'étiquette des déplacements (Scotty)
def update_deplacements_label():
    label_deplacements.config(text=f"Déplacements : {deplacements}")


# Fonction pour afficher le nombre de déplacements sur le canevas (Scotty)
def affiche_deplacements_canevas():
    Canevas.create_text(900, 750, fill='darkblue', font="Times 15", text=f"Déplacements : {deplacements}")


# Création d'un widget Bouton (bouton Réinitialiser) (Scotty)
BoutonReset = Button(Mafenetre, text="Réinitialiser les déplacements", command=reset_counter)
BoutonReset.grid(row=2, column=0)

# Création d'une étiquette pour afficher le nombre de déplacements (Scotty)
label_deplacements = Label(Mafenetre, text="Déplacements : 0", font="Times 15")
label_deplacements.grid(row=0, column=0)

# Création d'un widget Bouton pour muter la musique (Scotty)
BoutonMute = Button(Mafenetre, text="Mute/Unmute musique", command=toggle_mute)
BoutonMute.grid(row=4, column=0)

# Création d'un widget Bouton (bouton Quitter)  (Scotty)
BoutonQuitter = Button(Mafenetre, text="Quitter", command=Mafenetre.destroy)
BoutonQuitter.grid(row=1, column=0)

#Fonction pour recommancer le niveau      (Mathieu)
def recommencer_niveau():
    global numero_niveau, fini, deplacements, temps_niv_1, temps_niv_2, temps_niv_3, temps_tot, points
    #numero_niveau = 0
    fini = False
    deplacements = 0
    reset_counter()
    if numero_niveau == 0:
        for i in range(2, 10):
            for j in range(2, 14):
                plateau[i][j][0] = 0
                plateau[i][j][1] = 0
                plateau[i][j][2] = 0
                plateau[i][j][3] = 0
        genre_niveau_1()
        temps_niv_1 = 0
    elif numero_niveau == 1 :
        genere_niveau_2()
        temps_niv_2 =0
    elif numero_niveau == 2 :
        temps_niv_1, temps_niv_2, temps_niv_3, points, temps_tot = 0, 0, 0, 0
        genere_niveau_3()
    affiche_plateau_canevas()
    
# Création d'un bouton pour modifier les touches - Mathieu Scotty
BoutonSettings = Button(Mafenetre, text="Key Settings", command=set_key_bindings)
BoutonSettings.grid(row=5, column=0) 

#Création d'un bouton recommencer
BoutonRecommencer = Button(Mafenetre, text="Recommencer le niveau", command=recommencer_niveau)#Mathieu
BoutonRecommencer.grid(row=3, column=0)

# Création d'un widget Canvas(zone graphique)
Canevas = Canvas(Mafenetre, width=1000, height=800, bg='grey')  #Mathieu 
Canevas.create_text(500, 100, fill="darkblue", font="Times 60 italic bold", text="SOKOBAN")
Canevas.create_text(500, 250, fill="darkblue", font="Times 20", text="Poussez les caisses sur interrupteurs")
Canevas.create_text(500, 300, fill="darkblue", font="Times 20", text="Appuyez sur une touche pour commencer")

image_briques = PhotoImage(file="briques.png") # Simon
image_herbe = PhotoImage(file="herbe.png") # Simon

def affiche_plateau_canevas():
    for i in range(12):
        Canevas.create_line(0, (800 / 12) * i, 1000, (800 / 12) * i, width=0.5) #Mathieu
    for j in range(16):
        Canevas.create_line((1000 / 16) * j, 0, (1000 / 16) * j, 800, width=0.5)  #Mathieu
    for i in range(12):
        for j in range(16):
            if plateau[i][j][0] == 1:
                # Affichage mur
                Canevas.create_image((1000 / 16) * j + (1000 / 32), 
                                     (800 / 12) * i + (800 / 24), 
                                     image=image_briques) # Simon
            elif plateau[i][j][1] == 1:
                # Affichage joueur (carré pousseur)
                Canevas.create_text((1000 / 16) * j + (1000 / 32), (800 / 12) * i + (800 / 24),
                                     text="🤖", font="Arial 30 bold")
            elif plateau[i][j][2] == 1:
                # Affichage caisse (carré rouge)
                Canevas.create_rectangle((1000 / 16) * j, (800 / 12) * i,
                                         (1000 / 16) * j + (1000 / 16),
                                         (800 / 12) * i + (800 / 12), fill='#5C3D21')  #Mathieu & Scotty
            elif plateau[i][j][3] == 1:
                # Affichage interrupteur (rond rouge)
                taille_rond = (1000 / 16) / 2 + 20  # Augmentez la valeur ici pour agrandir les ronds
                Canevas.create_oval((1000 / 16) * j + taille_rond, (800 / 12) * i + taille_rond,
                                     (1000 / 16) * j + (1000 / 16) - taille_rond,
                                     (800 / 12) * i + (800 / 12) - taille_rond,
                                     outline='black', fill='red')
                
            else:
                    # Affichage de l'herbe sur les cases initialement grises
                    Canevas.create_image((1000 / 16) * j + (1000 / 32), 
                                         (800 / 12) * i + (800 / 24), 
                                         image=image_herbe)                            # Simon 
                
    update_deplacements_label()
    affiche_deplacements_canevas()   
    
def affiche_message_bravo():
    Canevas.create_text(500, 400, fill='darkblue', font="Times 50 italic bold", text=f"BRAVO !!!")
    Canevas.create_text(500, 500, fill='darkblue', font="Times 30 italic bold", text=f"\n \n Votre nombre total de déplacement est de {Dep_glob}")
    Canevas.create_text(500, 600, fill='darkblue', font="Times 30 italic bold", text=f"\n \n Votre nombre total de points est de {points}")
                
def Clavier(event):
    global numero_niveau, fini, deplacements

    if event.keysym in key_bindings.values():
        if fini == False:
            Canevas.delete("all")
            mvt_poss = True
            touche = event.keysym
            for i in range(12):
                for j in range(16):
                    if plateau[i][j][1] == 1 and mvt_poss == True:
                        if touche == key_bindings['Up'] and plateau[i-1][j][0] != 1 and not (plateau[i-1][j][2] == 1 and (plateau[i-2][j][2] == 1 or plateau[i-2][j][0] == 1)):
                            if plateau[i-1][j][2] == 1:
                                plateau[i-2][j][2] = 1
                                plateau[i-1][j][2] = 0
                            plateau[i][j][1] = 0
                            plateau[i-1][j][1] = 1
                            deplacements += 1
                        elif touche == key_bindings['Left'] and plateau[i][j-1][0] != 1 and not (plateau[i][j-1][2] == 1 and (plateau[i][j-2][2] == 1 or plateau[i][j-2][0] == 1)):
                            if plateau[i][j-1][2] == 1:
                                plateau[i][j-2][2] = 1
                                plateau[i][j-1][2] = 0
                            plateau[i][j][1] = 0
                            plateau[i][j-1][1] = 1
                            deplacements += 1
                        elif touche == key_bindings['Right'] and plateau[i][j+1][0] != 1 and not (plateau[i][j-2][2] == 1 and (plateau[i][j+2][2] == 1 or plateau[i][j+2][0] == 1)):
                            if plateau[i][j+1][2] == 1:
                                plateau[i][j+2][2] = 1
                                plateau[i][j+1][2] = 0
                            plateau[i][j][1] = 0
                            plateau[i][j+1][1] = 1
                            deplacements += 1
                        elif touche == key_bindings['Down'] and plateau[i+1][j][0] != 1 and not (plateau[i+1][j][2] == 1 and (plateau[i+2][j][2] == 1 or plateau[i+2][j][0] == 1)):
                            if plateau[i+1][j][2] == 1:
                                plateau[i+2][j][2] = 1
                                plateau[i+1][j][2] = 0
                            plateau[i][j][1] = 0
                            plateau[i+1][j][1] = 1
                            deplacements += 1
                        elif touche == key_bindings['Restart'] : # --- #D003
                            recommencer_niveau()
                        elif touche == key_bindings['Quit']: # --- #D003
                            Mafenetre.destroy
                        mvt_poss = False
    if event.keysym in key_bindings.values(): # --- #D003 -- Fix des key_bindings 'Restart' et 'Quit' qui ne fonctionnaient pas
        touche = event.keysym
        if touche == key_bindings['Restart'] :
            recommencer_niveau()
        if touche == key_bindings['Quit'] :
            Mafenetre.destroy()
                        


        if test_victoire() == True:
            global Dep_glob, numero_niveau, temps_tot, points
            numero_niveau = numero_niveau + 1
            Dep_glob += deplacements
            deplacements=0
            temps_tempo = time_ns()
            if numero_niveau == 1:
                temps_tot += temps_tempo - temps_niv_1 
                points+=30
                genere_niveau_2()
            if numero_niveau == 2:
                temps_tot +=  temps_tempo - temps_niv_2
                points+=60
                genere_niveau_3()
                fini = True

        affiche_plateau_canevas()
        update_deplacements_label() #Scotty
        affiche_deplacements_canevas() #Scotty
        affiche_plateau_canevas() #Scotty
        
        if fini == True:                # Simon ( pour qu'il s'écrive par dessus l'herbe il fallait le mettre à la fin )
            numero_niveau=0             # Martin ( Permet de recommencer le jeu lorsque ce dernier est fini )
            temps_tot=temps_tot * 10e-7 //1000 +1
            points = (points*1.1*Dep_glob)// 15
            affiche_message_bravo()     
            Dep_glob, deplacements = 0, 0
            print(temps_tot) # -- Test console
            points=0

Canevas.focus_set()
Canevas.bind('<Key>', Clavier)
Canevas.grid(row=0, column=0)

Mafenetre.mainloop()

