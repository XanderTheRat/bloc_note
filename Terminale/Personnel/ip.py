def IP(ipv4):
    
    assert type(ipv4)==str
    nombre_point=0
    for _ in ipv4:
        if _=='.':
            nombre_point+=1
    assert nombre_point==3, 'IP Incorrecte'
    
    ip_tempo=''
    variable=0
    ip_octet=''
    expo=7
    
    for _ in ipv4:
        if _ == '.' or _ == '/' or _ == ' ':
            if not _ == ' ':
                var_tempo=int(ip_tempo)
                for i in range(8):
                    if var_tempo-2**expo >= 0:
                        var_tempo=var_tempo-2**expo
                        ip_octet+='1'
                        expo-=1
                    else :
                        ip_octet+='0'
                        expo-=1
                        
            if _=='.':
                ip_octet+='.'
            else :
                ip_octet+='/'
            variable=0
            ip_tempo=''
            expo=7
            
        elif variable <= 3:
            ip_tempo+=_
            variable+=1
        
        else :
            return 'IP incorrecte'
        
    var2=ipv4[::-1]
    
    for _ in var2 :
        if _ == '.' :
            var_tempo=int(ip_tempo)
            for i in range(8):
                if var_tempo-2**expo >= 0:
                    var_tempo=var_tempo-2**expo
                    ip_octet+='1'
                    expo-=1
                else :
                    ip_octet+='0'
                    expo-=1
                    
            ip_tempo=''
            return ip_octet
        


def Adresse_reseau(ipv4):
    masque=None

    for var in ipv4:
        if var=='/':
            masque=ipv4.split('/')
    if masque==None :
        return 'Probleme de masque dans votre IP'

    nombre_masque=int(masque[1])
    assert 1<=nombre_masque<=32, "Erreur de masque dans votre IP"

    ip_split=IP(masque[0]).split('.')
    ip=ip_split[0]+ip_split[1]+ip_split[2]+ip_split[3]
    diff_parties=[ip[:nombre_masque],ip[nombre_masque:]]
    diff_parties[0]=diff_parties[0].replace('0','1')
    ip=diff_parties[0]+diff_parties[1]

    if nombre_masque==32 :
        print("A quoi sert un tel masque ?")
    if nombre_masque==1:
        print("Quel masque inutile")
    return ip[:8]+'.'+ip[8:16]+'.'+ip[16:24]+'.'+ip[24:]
    
    
    
    
def ipcli():
    arg=input(str('Rentrez votre IP Client\n'))
    print(f"L'ip binaire de cette adresse IP est :\n {IP(arg)}")

def ipres():
    arg=input(str('Rentrez votre IP Réseau\n'))
    print(f"L'ip réseau de cette adresse IP est :\n {Adresse_reseau(arg)}")

def coupleip():
    ipcli()
    print('\n')
    ipres()